/* global describe, it, before, after, beforeEach, afterEach */
/* eslint func-names: off, prefer-arrow-callback: off, no-unused-vars: off */
const should = require('chai').should();
const { Client, Request, toXml, parseXml, cleanPSObject } = require('..');
const config = Object.assign({}, require('./config.json'), require('./credentials.json'));

const dump = obj => console.log(require('util').inspect(obj, { depth: null, compact: true, colors: true })); // eslint-disable-line
// End common, tests below

describe('XML JS Conversion (xml.js)', function () {
  describe('toXml()', function () {
    const typesXmlTest = config.tests.xml.types;
    let xml;

    it('Convert JS object to XML string', async function () {
      xml = await toXml('obj', typesXmlTest.obj);
      xml.should.be.a('string');
    });

    it('XML string looks like valid XML', function () {
      /^<\?xml/.test(xml).should.equal(true);
    });

    it('XML string should be match the expected output', function () {
      xml.should.equal(typesXmlTest.xml);
    });
  });

  describe('parseXml()', function () {
    const typesXmlTest = config.tests.xml.types;
    let obj;

    it('Convert XML string to JS Object', async function () {
      const o = await parseXml(typesXmlTest.xml);
      o.should.be.a('object');
      o.should.have.property('obj');
      obj = o.obj; // eslint-disable-line
    });

    it('JS Object has correct keys', function () {
      obj.should.have.property('lowercase');
      obj.should.have.property('UPPERCASE');
      obj.should.have.property('MixedCase');
      obj.should.have.property('nested');
      obj.should.have.property('nestedArray');
      obj.should.not.have.property('LOWERCASE');
    });

    it('JS Object arrays have correct length', function () {
      obj.array.should.have.length(3);
      obj.nestedArray.should.have.length(2);
    });
  });

  describe('cleanPSObject()', function () {
    const typesXmlTest = config.tests.xml.types;
    let obj;

    it('Clean JS Object', async function () {
      const o = await parseXml(typesXmlTest.xml);
      o.should.be.a('object');
      o.should.have.property('obj');
      obj = cleanPSObject(o.obj);
    });

    it('Single array values compressed', function () {
      obj.lowercase.should.be.a('string');
      obj.UPPERCASE.should.be.a('string');
      obj.MixedCase.should.be.a('string');
    });

    it('Single key objects compressed', function () {
      obj.nestedArray[0].should.be.a('string');
      obj.nestedArray[1].should.be.a('string');
    });
  });
});
