/* global describe, it, before, after, beforeEach, afterEach */
/* eslint func-names: off, prefer-arrow-callback: off, no-unused-vars: off */
const should = require('chai').should();
const { Client, Request, toXml, parseXml, cleanPSObject } = require('..');
const config = Object.assign({}, require('./config.json'), require('./credentials.json'));

const dump = obj => console.log(require('util').inspect(obj, { depth: null, compact: true, colors: true })); // eslint-disable-line
// End common, tests below

describe('Client (client.js)', function () {
  beforeEach(function () {
    this.client = new Client(config);
  });

  afterEach(function () {
    delete this.client;
  });

  describe('Constructor', function () {
    it('IB End Point is required', function () {
      const badConfig = Object.assign({}, config);
      delete badConfig.IBEndPoint;
      should.throw(() => new Client(badConfig));
    });

    it('Requesting Node and/or Origin Node is required', function () {
      const badConfig = Object.assign({}, config);
      delete badConfig.requestingNode;
      delete badConfig.origNode;
      should.throw(() => new Client(badConfig));
    });

    it('Missing Requesting Node or Origin Node is filled in', function () {
      let testConfig = Object.assign({}, config);
      delete testConfig.requestingNode;
      let testClient = new Client(testConfig);
      testClient.requestingNode.should.equal(testClient.origNode);

      testConfig = Object.assign({}, config);
      delete testConfig.origNode;
      testClient = new Client(testConfig);
      testClient.origNode.should.equal(testClient.requestingNode);
    });

    it('Destination Node is required or filled in from IB URI', function () {
      const badConfig = Object.assign({}, config);
      delete badConfig.destinationNode;
      should.throw(() => new Client(badConfig));

      const testConfig = Object.assign({}, config);
      delete testConfig.destinationNode;
      testConfig.IBEndPoint = `${testConfig.IBEndPoint}?To=TEST_NODE`;
      const testClient = new Client(testConfig);
      testClient.destinationNode.should.equal('TEST_NODE');
    });
  });

  describe('Requests', function () {
    describe('Operation not defined', function () {
      beforeEach(function () {
        this.params = config.tests.requests.operationNotDefined;
        this.request = this.client.call(this.params.operation);
      });

      it('Request should return an error', function (done) {
        this.request
          .then((response) => {
            should.not.exist(response);
          })
          .catch((err) => {
            should.exist(err);
            err.message.should.include('Operation not setup');
          })
          .then(done);
      });
      // TODO test formatting of response XML?
    });

    describe('Simple', function () {
      beforeEach(function () {
        this.params = config.tests.requests.simple;
        this.request = this.client.call(this.params.operation);
      });

      it('Response should be an array of objects', function (done) {
        this.request
          .then((response) => {
            should.exist(response);
            response.should.be.an.instanceOf(Array);
            response.length.should.be.at.least(1);
            response.forEach((o) => {
              o.should.be.an('object');
            });
          })
          .then(done);
      });

      it('Response objects should have the same keys ', function (done) {
        this.request
          .then((response) => {
            response.forEach((o) => {
              o.should.have.all.keys(response[0]);
            });
          })
          .then(done);
      });

      // TODO test formatting of response XML?
    });

    describe('Parameters', function () {
      beforeEach(function () {
        this.params = config.tests.requests.parameter;
        this.request = this.client.call(this.params.operation, this.params.params);
      });

      it('Response should be an array of objects', function (done) {
        this.request
          .then((response) => {
            should.exist(response);
            response.should.be.an.instanceOf(Array);
            response.length.should.be.at.least(1);
            response.forEach((o) => {
              o.should.be.an('object');
            });
          })
          .then(done);
      });

      it('Response objects should have the same keys ', function (done) {
        this.request
          .then((response) => {
            response.forEach((o) => {
              o.should.have.all.keys(response[0]);
            });
          })
          .then(done);
      });

      // TODO test formatting of response XML?
    });
  });

  describe('Caching', function () {
    beforeEach(function () {
      delete this.client;
      this.client = new Client(Object.assign({}, config, { cache: true }));
      this.params = config.tests.requests.simple;
      this.request = this.client.call(this.params.operation)
        .then((response) => {
          this.cacheKey = this.client.lastRequest.hash;
          return response;
        });
    });

    it('Cache should start empty', function () {
      this.client.cache.length.should.equal(0);
    });

    it('Cache should cache requests', function (done) {
      this.client.call(this.params.operation)
        .then((response) => {
          this.client.cache.keys().should.include(this.cacheKey);
        })
        .then(done);
    });
  });
});
