/* global describe, it, before, after, beforeEach, afterEach */
/* eslint func-names: off, prefer-arrow-callback: off, no-unused-vars: off */
const should = require('chai').should();
const { Client, Request, toXml, parseXml, cleanPSObject } = require('..');
const config = Object.assign({}, require('./config.json'), require('./credentials.json'));

const dump = obj => console.log(require('util').inspect(obj, { depth: null, compact: true, colors: true })); // eslint-disable-line
// End common, tests below

describe('Request (request.js)', function () {
  beforeEach(function () {
    this.req = new Request('getData', { paramKey: 'paramValue' });
  });

  afterEach(function () {
    delete this.req;
  });

  describe('Basic', function () {
    it('Operation is required', function () {
      should.throw(() => new Request());
    });

    it('Parameters are correct', function () {
      this.req.operation.should.equal('getData');
      this.req.params.should.deep.equal({
        operation: 'getData',
        paramKey: 'paramValue',
      });
    });

    it('reset() clears properties', function () {
      this.req.response = 'string';
      this.req.reset();
      should.not.exist(this.req.response);
    });
  });

  describe('Parameters', function () {
    it('extraParams and packageParams work as expected', function () {
      this.req.reset();

      this.req.packageParams.should.deep.equal({
        operation: 'getData',
        paramKey: 'paramValue',
      });

      this.req.extraParams = {
        extra: 'extraValue',
      };

      this.req.packageParams.should.deep.equal({
        operation: 'getData',
        paramKey: 'paramValue',
        extra: 'extraValue',
      });
    });
  });

  describe('XML', function () {
    it('xml() creates expected xml', function () {
      this.req.xml.should.equal('<?xml version="1.0" encoding="utf-8"?>\n<nau_ows><operation>getData</operation><paramKey>paramValue</paramKey></nau_ows>');
    });

    it('packageXml() creates expected xml', function () {
      this.req.extraParams = {
        extra: 'extraValue',
      };

      this.req.packageXml.should.equal('<?xml version="1.0" encoding="utf-8"?>\n<nau_ows><operation>getData</operation><paramKey>paramValue</paramKey><extra>extraValue</extra></nau_ows>');
    });

    it('hash() is a string', function () {
      this.req.hash.should.be.a('string');
    });
  });
});
