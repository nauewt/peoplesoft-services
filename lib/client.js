const crypto = require('crypto');
const lru = require('lru-cache');
const request = require('request');
const util = require('util');
const uuid = require('uuid/v4');

const { Request } = require('./request');
const { toXmlSync, parseXml, cleanPSObject } = require('./xml');

const hash = text => crypto.createHash('sha1').update(text, 'utf16le').digest('base64');

const dump = obj => console.log(util.inspect(obj, { depth: null, colors: true })); // eslint-disable-line

class Client {
  constructor(options = {}) {
    // Normalize options
    const opts = {};
    Object.keys(options).forEach((k) => {
      opts[k.toLowerCase()] = options[k];
    });

    // Set options
    this.debug = opts.debug
      ? (...args) => {
        args.map(typeof opts.debug === 'function' ? opts.debug : dump);
      }
      : () => {};

    this.cache = opts.cache
      ? lru({
        max: 100,
        maxAge: 1000 * 60 * 60,
      })
      : null;

    this.cacheWhitelist = opts.cachewhitelist;
    this.cacheBlacklist = this.cachewhitelist ? null : opts.cacheblacklist;

    this.IB = opts.ibendpoint || opts.ib;
    this.externalOperationName = opts.externaloperationname || 'N__WS_LIBRARY.v1';
    this.requestingNode = opts.requestingnode || opts.orignode;
    this.origNode = opts.orignode || opts.requestingnode;
    this.destinationNode = opts.destinationnode;
    this.password = opts.password || '';

    this.hash = opts.hash !== false; // default true
    this.secret = opts.secret || '';

    this.uid = opts.uid || null;
    this.version = opts.version || 1;

    this.lastRequest = null;

    // Validate options
    if (!this.IB) throw new Error('Integration Broker end point is required (IB)');
    if (!this.origNode && !this.requestingNode) throw new Error('Requesting Node and/or Origin Node is required');

    if (!this.origNode) {
      this.origNode = this.requestingNode;
    }

    if (!this.requestingNode) {
      this.requestingNode = this.origNode;
    }

    if (!this.destinationNode) {
      const to = this.IB.match(/To=(\w+)/i);

      if (to && to[1]) {
        this.destinationNode = to[1]; // eslint-disable-line
      } else {
        throw new Error('Destination Node is required (DestinationNode)');
      }
    }
  }

  /**
   * Call a PS webservice by name
   * @param {string} operation Name of the operation to call
   * @param {object} params Parameters for the call in the format { string: string }
   */
  call(operation, params = {}) {
    const req = new Request(operation, params);
    return this.sendRequest(req);
  }

  /**
   * Sends the request to PeopleSoft and routes the response to handlers
   * @param  {Request} req   Request object
   */
  sendRequest(req) {
    if (typeof req.reset !== 'function') throw new Error('sendRequest requires a Request instance');

    this.lastRequest = req;
    req.reset();

    // If the package is already a string (XMl), send as is
    const cacheKey = this.getCacheKey(req);
    const requestXml = this.buildRequest(req);

    req.requestXml = requestXml;

    // If caching is enabled, check the cache for a previous identical request
    // Short-circuit the response if found.
    if (this.cache && cacheKey) {
      const cachedResponse = this.cache.get(cacheKey);

      if (cachedResponse) {
        this.debug(`[peoplesoft-services] Client::sendRequest() -> CACHED RESPONSE for ${cacheKey}`, cachedResponse);
        req.response = cachedResponse;
        return Promise.resolve(cachedResponse);
      }
    }

    return this.sendRequestXml(requestXml)
      .then((responseXml) => {
        req.responseXml = responseXml;
        return parseXml(responseXml);
      })
      .then(response => this.parseResponse(response))
      .then((response) => {
        req.response = response;

        if (this.cache && cacheKey) {
          this.debug(`[peoplesoft-services] Client::sendRequest() -> CACHING ${cacheKey}`);
          this.cache.set(cacheKey, response);
        }

        return response;
      })
      .catch((err) => {
        req.error = err;

        if (this.cache && cacheKey) {
          this.debug(`[peoplesoft-services] Client::sendRequest() -> ERROR, DELETING CACHE ${cacheKey}`);
          this.cache.del(cacheKey);
        }

        if (false) { // TODO: Detect XML parsing fail
          this.debug('[peoplesoft-services] Client::sendRequest() -> XML ERROR', err);
          const newErr = new Error('Bad XML response from IB');
          newErr.originalError = err;
          throw newErr;
        }

        throw err;
      });
  }

  sendRequestXml(requestXml) {
    this.debug('[peoplesoft-services] Client::sendRequest()', requestXml);

    return new Promise((resolve, reject) => {
      request({
        method: 'POST',
        uri: this.IB,
        headers: { 'Content-Type': 'application/xml' },
        // forever: true,
        agentOptions: {
          rejectUnauthorized: false,
          // secureProtocol: 'SSLv3_method',
          // ciphers: 'AES128-SHA:AES256-SHA:RC4-MD5:RC4-SHA'
        },
        body: requestXml,
      }, (err, response, body) => {
        this.debug(`[peoplesoft-services] Client::callIB() -> RESULT ${response && response.statusCode ? response.statusCode : 'null'}`, body);

        if (err || !response || !body || response.statusCode !== 200) {
          this.debug('[peoplesoft-services] Client::callIB() -> ERROR', err);
          const newErr = new Error('Bad response from IB');
          newErr.originalError = err;

          reject(newErr);
        } else {
          resolve(body);
        }
      });
    });
  }

  // Build Request Methods

  /**
   * Builds the entire xml package for a request
   * @param {Request} req
   */
  buildRequest(req) {
    if (!req.params) throw new Error('buildRequest requires a Request instance');

    req.extraParams = {
      version: this.version || '1',
      uid: this.uid || '',
    };

    if (this.hash) {
      // Hash the request
      const reqHash = this.getRequestHash(req.packageXml);
      req.extraParams.hash = reqHash.hash;
      req.extraParams.salt = reqHash.salt;
    }

    // Add timestamp to request
    req.extraParams.time = new Date().toISOString();

    this.debug('[peoplesoft-services] Client::buildRequest()', req.packageParams);

    // Construct the final package
    return this.buildRequestXml(req);
  }

  /**
   * Builds the actual XML request required by IB.
   * @param {Request} req
   */
  buildRequestXml(req) {
    if (!req.packageXml) throw new Error('buildRequestXml requires a Request instance');

    const pkg = {
      ExternalOperationName: this.externalOperationName,
      From: {
        RequestingNode: this.requestingNode,
        OrigNode: this.origNode,
        Password: this.password,
      },
      To: {
        DestinationNode: this.destinationNode,
      },
      ContentSections: {
        ContentSection: {
          Data: { '%': req.packageXml || '' },
        },
      },
    };

    return toXmlSync('IBRequest', pkg);
  }

  /**
   * Hashes an XML request
   * @param {string} text
   */
  getRequestHash(text) {
    const salt = uuid();
    const hashText = text + this.secret + salt;

    return {
      hash: hash(hashText),
      salt,
    };
  }

  /**
   * Clears cache either for the given request package or the entire thing.
   * @param {Request} req
   */
  clearCache(req = null) {
    if (this.cache) {
      if (req && req.hash) {
        this.cache.del(req.hash);
      } else {
        this.cache.reset();
      }
    }
  }

  /**
   * Returns the cache key for the request if caching is enabled
   * and the request is valid for caching.
   * @param {Request} req
   */
  getCacheKey(req) {
    // Caching must be enabled
    if (this.cache && req.operation) {
      // if whitelist and operation is not in whitelist, fail
      if (
        this.cacheWhitelist
        && Array.isArray(this.cacheWhitelist)
        && !this.cacheWhitelist.includes(req.operation)
      ) {
        this.debug(`Operation "${req.operation}" will not be cached (not whitelisted)`, this.cacheWhitelist);
        return null;
      }

      // if blacklist and operation is in blacklist, fail
      if (
        this.cacheBlacklist
        && Array.isArray(this.cacheBlacklist)
        && this.cacheBlacklist.includes(req.operation)
      ) {
        this.debug(`Operation "${req.operation}" will not be cached (blacklisted)`, this.cacheBlacklist);
        return null;
      }

      // everything ok, return a hash of the package as an id
      return req.hash;
    }

    // Package is not cacheable
    return null;
  }

  // Handle Response Methods

  /**
   * Parses IB responses into useful information
   * @param {object} response
   */
  parseResponse(response) {
    this.debug('[peoplesoft-services] Client::parseResponse()', response);

    if (response.PSMessage) {
      return this.parsePSMessage(response.PSMessage);
    }

    if (response.IBResponse) {
      return this.parseIBResponse(response.IBResponse);
    }

    if (response.ERROR) {
      this.debug('[peoplesoft-services] Client::parseResponse() :: ERROR');
      throw new Error(response.ERROR);
    }

    this.debug('[peoplesoft-services] Client::parseResponse() :: PSMessage');
    return response;
  }

  /**
   * Handles PSMessage responses
   */
  parsePSMessage(response) {
    this.debug('[peoplesoft-services] Client::parsePSMessage()');

    const obj = response.MsgData[0].transaction
    || response.MsgData[0].Transaction
    || response.MsgData[0].TRANSACTION;

    return cleanPSObject(obj);
  }

  /**
   * Handles IBResponse responses
   */
  parseIBResponse(response) {
    this.debug('[peoplesoft-services] Client::parseIBResponse()');

    // For now assume all of these are errors
    const err = new Error('Error from IB');

    Object.keys(response).forEach((k) => {
      err[k] = response[k];
    });

    err.code = err.statuscode;

    throw err;
  }
}

module.exports = {
  Client,
};
