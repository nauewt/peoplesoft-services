const data2xml = require('data2xml');
const xml2js = require('xml2js');

/**
 * Converts a standard JS object to XML
 */
const toXmlSync = data2xml({ cdataProp: '%' });

/**
 * Converts a standard JS object to XML, returns a Promise
 */
const toXml = async (tagName, obj) => Promise.resolve(toXmlSync(tagName, obj));

/**
 * Converts XML to a standard JS object, returns a Promise
 */
const parseXml = async xml => new Promise((resolve, reject) => {
  xml2js.parseString(xml, (err, obj) => {
    if (err) return reject(err);
    return resolve(obj);
  });
});

/**
 * Cleans up PeopleSoft rowset objects that have been converted from XML
 * See: Client::parsePSMessage()
 * @param {object} obj
 */
const cleanPSObject = (obj) => {
  // Arrays
  if (Array.isArray(obj)) {
    if (obj.length === 1 && typeof obj[0] !== 'object') {
      return obj[0];
    }
    return obj.map(cleanPSObject);
  }
  // Obj
  if (typeof obj === 'object') {
    const keys = Object.keys(obj);
    let cleaned;

    if (keys.length === 1) {
      cleaned = cleanPSObject(obj[keys[0]]);
      return (Array.isArray(cleaned) && cleaned.length === 1)
        ? cleaned[0]
        : cleaned;
    }

    cleaned = {};
    keys.forEach((k) => {
      cleaned[k] = cleanPSObject(obj[k]);
    });
    return cleaned;
  }
  // Something else
  return obj;
};

module.exports = {
  toXml,
  toXmlSync,
  parseXml,
  cleanPSObject,
};
