const objectHash = require('object-hash');

const { toXmlSync } = require('./xml');

class Request {
  constructor(operation, params = {}) {
    if (!operation) throw new Error('Operation is required');

    this.params = Object.assign({ operation }, params);
    this.reset();
  }

  reset() {
    this.extraParams = null;
    this.requestXml = null;
    this.response = null;
    this.responseXml = null;
    this.error = null;
  }

  toObject() {
    return {
      operation: this.operation,
      params: this.params,
      xml: this.xml,
      extraParams: this.extraParams,
      packageParams: this.packageParams,
      packageXml: this.packageXml,
      requestXml: this.requestXml,
      response: this.response,
      responseXml: this.responseXml,
      error: this.error,
      hash: this.hash,
    };
  }

  get operation() {
    return this.params.operation;
  }

  get xml() {
    return toXmlSync(Request.tagName, this.params);
  }

  get packageParams() {
    return Object.assign({}, this.params, this.extraParams || {});
  }

  get packageXml() {
    return toXmlSync(Request.tagName, this.packageParams);
  }

  get hash() {
    return objectHash(this.params);
  }

  get success() {
    return !this.error;
  }
}

Request.tagName = 'nau_ows';


module.exports = {
  Request,
};
